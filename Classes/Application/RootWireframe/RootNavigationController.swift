//
//  RootNavigationController.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class RootNavigationController: UINavigationController {
    
    fileprivate var route: Route {
        didSet {
            setNavigationBarHidden(isNavigationHidden, animated: true)
        }
    }
    
    convenience init(route: Route) {
        self.init(nibName: nil, bundle: nil)
        update(route: route, animated: false)
    }
    
    fileprivate override init(nibName nibNameOrNil: String?, bundle nibBundleOrNil: Bundle?) {
        self.route = .entrance
        super.init(nibName: nibNameOrNil, bundle: nibBundleOrNil)
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
}

extension RootNavigationController {
    
    func update(route: Route, animated: Bool = true) {
        DispatchQueue.toMain {
            self.route = route
            self.setViewControllers([route.viewController], animated: animated)
        }
    }
    
    fileprivate var isNavigationHidden: Bool {
        return route == .tabBar
    }
}
