//
//  RootWireframe.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import DefaultsKit
import FirebaseAuth

class RootWireframe {
    
    weak private(set) var window: UIWindow?
    
    init(window: UIWindow) {
        self.window = window
        window.backgroundColor = .white
        window.rootViewController = navigationController
    }
}

extension RootWireframe {
    
    func reset(to route: Route) {
        DispatchQueue.toMain {
            self.navigationController.update(route: route)
        }
    }
}

fileprivate extension RootWireframe {
    
    var navigationController: RootNavigationController {
        guard let root = window?.rootViewController as? RootNavigationController else {
            return RootNavigationController(route: initialRoute())
        }
        return root
    }
    
    private func initialRoute() -> Route {
        
        guard UserDefaults.isSignedIn else {
            return .entrance
        }
        
        return .tabBar
    }
}
