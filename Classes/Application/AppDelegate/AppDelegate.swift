//
//  AppDelegate.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?
    lazy var dependencies = AppDependencies()

    func application(_ app: UIApplication, didFinishLaunchingWithOptions options: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        self.appWindow.makeKey()
        AppDependencies.setup(app: app, options: options) {
            dependencies.update(window: self.appWindow)
            self.appWindow.makeKeyAndVisible()
        }
        
        return true
    }
}

extension AppDelegate {
    
    func reset(to route: Route) {
        dependencies.rootWireframe?.reset(to: route)
    }
    
    fileprivate var appWindow: UIWindow {
        guard let window = self.window else {
            self.window = UIWindow(frame: UIScreen.main.bounds)
            return self.window!
        }
        return window
    }
}

extension UIApplication {
    
    static var currentDelegate: AppDelegate {
        return UIApplication.shared.delegate as! AppDelegate
    }
}
