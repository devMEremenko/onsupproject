//
//  AppDependencies.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import Firebase

class AppDependencies {
    
    var rootWireframe: RootWireframe?
    
    func update(window: UIWindow) {
        rootWireframe = RootWireframe(window: window)
    }
    
    static func setup(app: UIApplication,
                      options: [UIApplicationLaunchOptionsKey: Any]?,
                      _ handler: Empty) {
        
        FirebaseApp.configure()
        
        handler()
    }
}
