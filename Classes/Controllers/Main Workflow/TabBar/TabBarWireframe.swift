//
//  TabBarWireframe.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class TabBarWireframe {
    
    private(set) var tabBar: TabBarController = {
        
        let tabBar = TabBarController()
        
        let catalogAssembly = CatalogAssembly()
        let catalogVC = catalogAssembly.wireframe.viewController
        
        let profileVC = ProfileAssembly().vc
        
        let ordersAssembly = OrdersAssembly()
        let ordersVC = ordersAssembly.wireframe.viewController
        
        catalogVC?.tabBarItem = UITabBarItem.catalog
        profileVC.tabBarItem = UITabBarItem.profile
        ordersVC?.tabBarItem = UITabBarItem.orders
        
        let controllers = [catalogVC, profileVC, ordersVC].compactMap({ $0 })
        let navigations = controllers.map({ UINavigationController(rootViewController: $0)})
        navigations.forEach({ nc in
            nc.navigationItem.largeTitleDisplayMode = .automatic
            nc.navigationBar.prefersLargeTitles = true
        })
        
        tabBar.viewControllers = navigations
        
        // preload tabs
        // _ = catalogVC?.view
        _ = profileVC.view
        _ = ordersVC?.view
        
        return tabBar
    }()
}
