//
//  ProfileInteractor.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class ProfileInteractor {
    
    weak var output: ProfileInteractorOutput?
    
    private lazy var updater = UserUpdater()
    private lazy var observer = UserObserver(observers: [self])
}

extension ProfileInteractor: ProfileInteractorInput {
    
    func loadUserData() {
        observer.startObserving()
    }
    
    func update(user: User) {
        
        updater.update(user, success: { [weak self] in
            DispatchQueue.toMain {
                self?.output?.userUpdated()
            }
        }) { [weak self] error in
            DispatchQueue.toMain {
                self?.output?.updateUserFailed(with: error)
            }
        }
    }
}

extension ProfileInteractor: FirebaseObserver {
    
    typealias Model = User
    
    func data(added models: [User]) {
        guard let user = models.first else { return }
        output?.userLoaded(user)
    }
    
    func data(changed models: [User]) {
        
    }
    
    func data(removed models: [User]) {
        
    }
}
