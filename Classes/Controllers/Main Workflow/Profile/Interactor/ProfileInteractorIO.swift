//
//  ProfileInteractorIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol ProfileInteractorInput: class {
    
    func loadUserData()
    func update(user: User)
}

protocol ProfileInteractorOutput: class {
    
    func userLoaded(_ user: User)
    func userUpdated()
    func updateUserFailed(with: Error)
}
