//
//  ProfileWireframe.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class ProfileWireframe {
    
    weak var viewController: UIViewController?
    weak var moduleInterface: ProfileModuleInput?
}

extension ProfileWireframe {
    
}
