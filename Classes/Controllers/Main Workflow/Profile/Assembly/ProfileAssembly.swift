//
//  ProfileAssembly.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class ProfileAssembly {
    
    let wireframe = ProfileWireframe()
    let vc = ProfileVC()
    
    private let presenter = ProfilePresenter()
    private let interactor = ProfileInteractor()
    
    init() {
        vc.eventHandler = presenter
        presenter.userInterface = vc
        presenter.wireframe = wireframe
        presenter.interactor = interactor

        wireframe.viewController = vc
        wireframe.moduleInterface = presenter

        interactor.output = presenter
    }
}
