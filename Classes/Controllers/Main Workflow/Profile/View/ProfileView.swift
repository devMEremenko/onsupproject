//
//  ProfileView.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class ProfileView: UIView {
    
    let table = UITableView(frame: .zero, style: .grouped)
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubview(table)
        table.rowHeight = 44.0
        table.keyboardDismissMode = .interactive
        table.snp.makeConstraints { maker in maker.edges.equalTo(self) }
    }
}
