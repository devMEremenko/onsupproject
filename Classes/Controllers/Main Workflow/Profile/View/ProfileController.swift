//
//  ProfileController.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import UIKit
import DTTableViewManager

protocol ProfileControllerDelegate: class {
    
    func userDataUpdated(_ user: User)
}

class ProfileController: DTTableViewManageable {
    
    var tableView: UITableView!
    fileprivate weak var delegate: ProfileControllerDelegate?
    
    init(table: UITableView, _ delegate: ProfileControllerDelegate?) {
        self.delegate = delegate
        tableView = table
        manager.startManaging(withDelegate: self)
        manager.register(TextFieldCell.self)
    }
    
    func setup(_ user: User? = nil) {
        
        manager.memoryStorage.removeAllItems()
        
        //TODO: localize
        let firstName = TextFieldCellViewModel(placeholder: "Имя")
        let lastName = TextFieldCellViewModel(placeholder: "Фамилия")
        let address = TextFieldCellViewModel(placeholder: "Адрес")
        let phone = TextFieldCellViewModel(placeholder: "Телефон",
                                           style: Theme.TextField.phone)
        
        firstName.text = user?.firstName
        lastName.text = user?.lastName
        address.text = user?.address
        phone.text = user?.phone
        
        let items = [firstName, lastName, address, phone]        
        manager.memoryStorage.addItems(items)
    }
    
    func updateUserData() {
        guard let items = manager.memoryStorage.items(inSection: 0) as? [TextFieldCellViewModel] else {
            return
        }
        
        let user = User()
        user.firstName = items[0].text
        user.lastName = items[1].text
        user.address = items[2].text
        user.phone = items[3].text
        
        delegate?.userDataUpdated(user)
    }
}
