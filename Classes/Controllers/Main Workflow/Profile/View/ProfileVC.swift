//
//  ProfileVC.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class ProfileVC: UIViewController {

    var eventHandler: ProfileModuleInput?
    fileprivate let contentView = ProfileView()
    fileprivate lazy var controller = ProfileController(table: contentView.table, self)
    
    override func loadView() {
        self.view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        title = String.loc("profile.title")
        controller.setup()
        eventHandler?.viewDidSetup()
        
        //TODO: loc
        navigationItem.rightBarButtonItem = UIBarButtonItem(title: "Сохранить") {
            self.contentView.endEditing(true)
            self.controller.updateUserData()
        }
        
        if eventHandler?.type == .general {
            navigationItem.leftBarButtonItem = UIBarButtonItem(title: "Выйти") {
                self.eventHandler?.logOut()
            }
        }
    }
}

extension ProfileVC: ProfileUserInterfaceInput {
    
    func userLoaded(_ user: User) {
        controller.setup(user)
    }
}

extension ProfileVC: ProfileControllerDelegate {
    
    func userDataUpdated(_ user: User) {
        eventHandler?.update(user: user)
    }
}
