//
//  ProfileUserInterfaceIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol ProfileUserInterfaceInput: BaseInterfaceInput {
    
    func userLoaded(_ user: User)
}

protocol ProfileUserInterfaceOutput: class {
    
}
