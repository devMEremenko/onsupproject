//
//  ProfilePresenter.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import EasyCalls
import FirebaseAuth

class ProfilePresenter {
    
    var interactor: ProfileInteractorInput?
    var wireframe: ProfileWireframe?
    weak var userInterface: ProfileUserInterfaceInput?
    weak var moduleOutput: ProfileModuleOutput?
    
    var type = ProfilePresentationType.general
}

enum ProfilePresentationType {
    case entrance
    case general
}

extension ProfilePresenter: ProfileModuleInput {
    
    func logOut() {
        tryCatch({
            try Auth.auth().signOut()
            UIApplication.currentDelegate.reset(to: .entrance)
        }) { error in
            self.userInterface?.show(error: error)
        }
    }
    
    func update(user: User) {
        interactor?.update(user: user)
    }
    
    func viewDidSetup() {
        interactor?.loadUserData()
    }
    
    func performEntranceFlow() {
        self.type = .entrance
    }
    
    func performMainFlow() {
        self.type = .general
    }
}

extension ProfilePresenter: ProfileInteractorOutput {
    
    func userUpdated() {
        if type == .entrance {
            UIApplication.currentDelegate.reset(to: .tabBar)
        }
    }
    
    func updateUserFailed(with error: Error) {
        userInterface?.show(error: error)
    }
    
    func userLoaded(_ user: User) {
        userInterface?.userLoaded(user)
    }
}
