//
//  ProfileModuleIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol ProfileModuleInput: BaseModuleInput {
    
    func performEntranceFlow()
    func performMainFlow()
    
    func logOut()
    func update(user: User)
    
    var type: ProfilePresentationType { get }
}

protocol ProfileModuleOutput: class {
    
}
