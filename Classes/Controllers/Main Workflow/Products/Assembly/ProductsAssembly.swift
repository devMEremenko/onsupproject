//
//  ProductsAssembly.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class ProductsAssembly {
    
    let wireframe = ProductsWireframe()
    
    private let vc = ProductsVC()
    private let presenter = ProductsPresenter()
    private let interactor = ProductsInteractor()
    
    init() {
        vc.eventHandler = presenter
        presenter.userInterface = vc
        presenter.wireframe = wireframe
        presenter.interactor = interactor

        wireframe.viewController = vc
        wireframe.moduleInterface = presenter

        interactor.output = presenter
    }
}
