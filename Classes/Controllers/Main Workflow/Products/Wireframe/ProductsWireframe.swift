//
//  ProductsWireframe.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class ProductsWireframe {
    
    weak var viewController: UIViewController?
    weak var moduleInterface: ProductsModuleInput?
    
    func presentCart() {
        
        DispatchQueue.toMain {
            self.viewController?.safePush(vc: CartAssembly().vc)
        }
    }
}
