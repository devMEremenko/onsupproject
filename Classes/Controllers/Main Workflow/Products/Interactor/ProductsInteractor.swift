//
//  ProductsInteractor.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class ProductsInteractor {
    
    weak var output: ProductsInteractorOutput?
    
    fileprivate var category: Category?
    fileprivate lazy var observer = ProductObserver(observers: [self],
                                                    categoryID: category?.id ?? "")
}

extension ProductsInteractor: ProductsInteractorInput {
    
    func setupForProductsPresentation(category: Category) {
        self.category = category
    }
    
    func loadProducts(searchQuery: String?) {
        observer.startObserving()
    }
    
    func product(selected product: Product) {
        UserDefaults.addCart(item: product)
    }
    
    func product(deselected product: Product) {
        UserDefaults.removeCart(item: product)
    }
}

extension ProductsInteractor: FirebaseObserver {
    
    typealias Model = Product
    
    func data(added models: [Product]) {
        output?.productsLoaded(products: models)
    }
    
    func data(changed models: [Product]) {
        ///TODO:
    }
    
    func data(removed models: [Product]) {
        ///TODO:
    }
}
