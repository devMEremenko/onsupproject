//
//  ProductsInteractorIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol ProductsInteractorInput: class {
    
    func setupForProductsPresentation(category: Category)
    func loadProducts(searchQuery: String?)
    
    func product(selected product: Product)
    func product(deselected product: Product)
}

protocol ProductsInteractorOutput: class {
    
    func productsLoaded(products: [Product])
    func loadProductsDidFail(error: Error)
}
