//
//  ProductsPresenter.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class ProductsPresenter {
    
    var interactor: ProductsInteractorInput?
    var wireframe: ProductsWireframe?
    weak var userInterface: ProductsUserInterfaceInput?
    weak var moduleOutput: ProductsModuleOutput?
}

extension ProductsPresenter: ProductsInteractorOutput {
    
    func productsLoaded(products: [Product]) {
        userInterface?.display(products: products)
    }
    
    func loadProductsDidFail(error: Error) {
        userInterface?.show(error: error)
    }
}

extension ProductsPresenter: ProductsModuleInput {
    
    func presentCart() {
        wireframe?.presentCart()
    }
    
    func viewDidSetup() {
        interactor?.loadProducts(searchQuery: nil)
    }
    
    func setupForProductsPresentation(category: Category) {
        interactor?.setupForProductsPresentation(category: category)
    }
    
    func product(selected product: Product) {
        interactor?.product(selected: product)
    }
    
    func product(deselected product: Product) {
        interactor?.product(deselected: product)
    }
}
