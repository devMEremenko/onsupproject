//
//  ProductsModuleIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol ProductsModuleInput: BaseModuleInput {
    
    func setupForProductsPresentation(category: Category)
    func presentCart()
    
    func product(selected product: Product)
    func product(deselected product: Product)
}

protocol ProductsModuleOutput: class {
    
}
