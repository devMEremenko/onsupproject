//
//  ProductsUserInterfaceIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol ProductsUserInterfaceInput: BaseInterfaceInput {
    
    func display(products: [Product])
}

protocol ProductsUserInterfaceOutput: class {
    
}
