//
//  ProductsController.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import DTModelStorage
import DTCollectionViewManager

protocol ProductsControllerOutput: class {
    
    func product(selected product: Product)
    func product(deselected product: Product)
}

class ProductsController: DTCollectionViewManageable {
    
    var collectionView: UICollectionView?
    weak var output: ProductsControllerOutput?
    
    private lazy var viewModels = [ProductCellModel]()
    
    init(_ collection: UICollectionView) {
        collectionView = collection
        
        manager.startManaging(withDelegate: self)
        manager.register(ProductCell.self)
    }
    
    func add(products: [Product]) {
        
        let viewModels = products.map({ product -> ProductCellModel in
            
            let model = ProductCellModel(product)
            model.selectionHandler = { product in
                self.output?.product(selected: product)
            }
            model.deselectionHandler = { product in
                self.output?.product(deselected: product)
            }
            return model
        })
        
        self.viewModels.append(contentsOf: viewModels)
        manager.memoryStorage.addItems(viewModels)
    }
    
    func update() {
        self.viewModels.forEach({ $0.refreshItemsCount() })
        manager.memoryStorage.reload(items: self.viewModels)
    }
}

extension MemoryStorage {
    
    func reload<T: Equatable>(items: [T], _ animated: Bool = true) {
        items.forEach({ self.reloadItem($0) })
    }
}
