//
//  ProductsVC.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class ProductsVC: UIViewController {

    var eventHandler: ProductsModuleInput?
    
    fileprivate let contentView = ProductsView()
    
    fileprivate lazy var controller: ProductsController = {
        let controller = ProductsController(contentView.collection)
        controller.output = self
        return controller
    }()
    
    override func loadView() {
        self.view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        title = String.loc("products.title")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem.cart {
            self.eventHandler?.presentCart()
        }
        
        eventHandler?.viewDidSetup()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        controller.update()
    }
}

extension ProductsVC: ProductsUserInterfaceInput {
    
    func display(products: [Product]) {
        controller.add(products: products)
    }
}

extension ProductsVC: ProductsControllerOutput {
    
    func product(selected product: Product) {
        eventHandler?.product(selected: product)
    }
    
    func product(deselected product: Product) {
        eventHandler?.product(deselected: product)
    }
}
