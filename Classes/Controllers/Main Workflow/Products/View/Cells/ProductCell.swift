//
//  ProductCell.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import DTModelStorage

class ProductCell: UICollectionViewCell, ModelTransfer {
    
    @IBOutlet weak var counterView: CounterView!
    
    @IBOutlet weak var productImageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    @IBOutlet weak var priceLabel: UILabel!
    
    typealias ModelType = ProductCellModel
        
    func update(with model: ProductCellModel) {
        
        let product = model.product
        nameLabel.text = product.name
        priceLabel.text = String(product.price ?? 0) + " | " + (product.unit ?? "")
        productImageView.pin_setImage(from: product.imageUrlString?.url)
        
        counterView.configure(model)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        productImageView.image = nil
    }
}

class ProductCellModel: CounterViewModuleInput, Equatable {
    
    typealias Handler = (Product) -> ()
    
    var selectionHandler: Handler?
    var deselectionHandler: Handler?
    
    var product: Product
    
    init(_ product: Product) {
        self.product = product
    }
    
    lazy var itemsCount: Int = {
        return UserDefaults.cartObserver.value.count(of: product)
    }()
    
    func refreshItemsCount() {
        itemsCount = UserDefaults.cartObserver.value.count(of: product)
    }
    
    func increase() {
        itemsCount = itemsCount + 1
        selectionHandler?(product)
    }
    
    func decrease() {
        guard itemsCount > 0 else { return }
        self.itemsCount = itemsCount - 1
        deselectionHandler?(product)
    }
    
    static func ==(lhs: ProductCellModel, rhs: ProductCellModel) -> Bool {
        return lhs.product == rhs.product
    }
}
