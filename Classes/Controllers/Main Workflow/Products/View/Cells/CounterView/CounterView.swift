//
//  CounterView.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

protocol CounterViewModuleInput: class {
    
    var itemsCount: Int { get set }
    
    func increase()
    func decrease()
}

class CounterView: UIView {
    
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var minusButton: UIButton!
    @IBOutlet weak var plusButton: UIButton!
    
    private(set) var model: CounterViewModuleInput?
    
    func configure(_ model: CounterViewModuleInput) {
        self.model = model
        countLabel.text = String(model.itemsCount)
    }
    
    @IBAction func minusSelected() {
        model?.decrease()
        countLabel.text = String(model?.itemsCount ?? 0)
    }
    
    @IBAction func plusSelected() {
        model?.increase()
        countLabel.text = String(model?.itemsCount ?? 0)
    }
}
