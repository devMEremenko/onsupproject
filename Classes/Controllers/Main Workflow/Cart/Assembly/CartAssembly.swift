//
//  CartAssembly.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class CartAssembly {
    
    let wireframe = CartWireframe()
    
    let vc = CartVC()
    private let presenter = CartPresenter()
    private let interactor = CartInteractor()
    
    init() {
        vc.eventHandler = presenter
        presenter.userInterface = vc
        presenter.wireframe = wireframe
        presenter.interactor = interactor

        wireframe.viewController = vc
        wireframe.moduleInterface = presenter

        interactor.output = presenter
    }
}
