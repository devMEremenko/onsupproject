//
//  CartWireframe.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import EasyCalls

class CartWireframe {
    
    weak var viewController: (UIViewController & CartUserInterfaceInput)?
    weak var moduleInterface: CartModuleInput?
}

extension CartWireframe {
    
    func handleSuccessOrder() {
        
        DispatchQueue.toMain {
            self.viewController?.navigationController?.popToRootViewController(animated: true)
            let message = String.loc("orders.order.has.been.created.alert")
            UIViewController.topVC?.show(message: message, actions: Action.ok)
        }
    }
}
