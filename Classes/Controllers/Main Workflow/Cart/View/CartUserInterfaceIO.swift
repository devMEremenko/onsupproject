//
//  CartUserInterfaceIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol CartUserInterfaceInput: BaseInterfaceInput {
    
    func display(products: [Product])
}

protocol CartUserInterfaceOutput: class {
    
}
