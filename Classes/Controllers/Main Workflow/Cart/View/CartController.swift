//
//  CartController.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import UIKit
import DTCollectionViewManager

protocol CartControllerOutput: class {
    
    func product(selected product: Product)
    func product(deselected product: Product)
}

class CartController: DTCollectionViewManageable {
    
    var collectionView: UICollectionView?
    weak var output: CartControllerOutput?
    fileprivate(set) lazy var products = [Product]()
    
    init(_ collection: UICollectionView) {
        collectionView = collection
        
        manager.startManaging(withDelegate: self)
        manager.register(ProductCell.self)
    }
    
    func add(products: [Product]) {
        self.products.append(contentsOf: products)
        
        let viewModels = convertToVieModels(from: products)
        manager.memoryStorage.addItems(viewModels)
    }
    
    private func convertToVieModels(from products: [Product]) -> [ProductCellModel] {
        
        var map = [Product: Int]()
        
        products.forEach { product in
            if let key = map[product] {
                map[product] = key + 1
            } else {
                map[product] = 1
            }
        }
        
        return map.map { product, count -> ProductCellModel in
            
            let model = ProductCellModel(product)
            model.itemsCount = count
            
            model.selectionHandler = { product in
                self.products.append(product)
                self.output?.product(selected: product)
            }
            model.deselectionHandler = { product in
                self.products.remove(item: product)
                self.output?.product(deselected: product)
            }
            return model
        }
    }
}
