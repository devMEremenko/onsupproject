//
//  CartVC.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class CartVC: UIViewController {

    var eventHandler: CartModuleInput?
    
    fileprivate let contentView = CartView()
    fileprivate lazy var controller: CartController = {
        let controller = CartController(contentView.collection)
        controller.output = self
        return controller
    }()

    override func loadView() {
        self.view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventHandler?.viewDidSetup()
        title = String.loc("cart.title")
        
        contentView.confirmButton.handler = { [weak self] in
            self?.eventHandler?.makeOrder()
        }
    }
}

extension CartVC: CartUserInterfaceInput {
    
    func display(products: [Product]) {
        contentView.update(with: products)
        controller.add(products: products)
    }
}

extension CartVC: CartControllerOutput {
    
    func product(selected product: Product) {
        contentView.update(with: controller.products)
        eventHandler?.product(selected: product)
    }
    
    func product(deselected product: Product) {
        contentView.update(with: controller.products)
        eventHandler?.product(deselected: product)
    }
}
