//
//  CartView.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class CartView: UIView {
    
    let collection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 0.5
        
        let windowSize = UIApplication.shared.keyWindow?.bounds.size ?? .zero
        let width = (windowSize.width / 2).floor - layout.minimumInteritemSpacing
        
        layout.itemSize = CGSize(width: width, height: 200)
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.accept(style: Theme.ScrollView.default)
        return collection
    }()
    
    let confirmButton: UIButton = {
        let button = UIButton(style: Theme.Buttons.default)
        button.setTitle("Подтвердить", for: .normal)
        return button
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        setup()
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
        setup()
    }
    
    func update(with products: [Product]) {
        let price = products.reduce(0, { $0 + ($1.price ?? 0) })
        confirmButton.setTitle("Подтвердить " + "(\(price) грн)", for: .normal)
    }
}

private extension CartView {
    
    func setup() {
        
        let buttonHeight: CGFloat = 44
        
        addSubview(collection)
        addSubview(confirmButton)
        
        collection.snp.makeConstraints { maker in
            maker.edges.equalTo(self).inset(UIEdgeInsetsMake(0, 0, buttonHeight, 0))
        }
        
        confirmButton.snp.makeConstraints { maker in
            maker.left.right.equalTo(self)
            maker.bottom.equalTo(self.snp.bottomMargin)
            maker.height.equalTo(buttonHeight)
        }
    }
}
