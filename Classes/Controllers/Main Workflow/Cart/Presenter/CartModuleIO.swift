//
//  CartModuleIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol CartModuleInput: BaseModuleInput {
    
    func product(selected product: Product)
    func product(deselected product: Product)
    
    func makeOrder()
}

protocol CartModuleOutput: class {
    
}
