//
//  CartPresenter.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class CartPresenter {
    
    var interactor: CartInteractorInput?
    var wireframe: CartWireframe?
    weak var userInterface: CartUserInterfaceInput?
    weak var moduleOutput: CartModuleOutput?
}

extension CartPresenter: CartInteractorOutput {
    
    func orderSuccessfullyCreated() {
        wireframe?.handleSuccessOrder()
    }
    
    func makeOrderDidFailWith(error: Error) {
        userInterface?.show(error: error)
    }
    
    func cartItemsLoaded(products: [Product]) {
        userInterface?.display(products: products)
    }
}

extension CartPresenter: CartModuleInput {
    
    func viewDidSetup() {
        interactor?.loadCartItems()
    }
    
    func product(selected product: Product) {
        interactor?.product(selected: product)
    }
    
    func product(deselected product: Product) {
        interactor?.product(deselected: product)
    }
    
    func makeOrder() {
        interactor?.makeOrder()
    }
}
