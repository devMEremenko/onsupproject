//
//  CartInteractor.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation
import Observable

class CartInteractor {
    
    weak var output: CartInteractorOutput?
    
    private var disposable: Disposable?
    private lazy var updater = OrderUpdater()
}

extension CartInteractor: CartInteractorInput {
    
    func loadCartItems() {
        let storage = UserDefaults.cartObserver.value
        self.output?.cartItemsLoaded(products: storage.items)
    }
    
    func product(selected product: Product) {
        UserDefaults.addCart(item: product)
    }
    
    func product(deselected product: Product) {
        UserDefaults.removeCart(item: product)
    }
    
    func makeOrder() {
        
        let items = UserDefaults.cartObserver.value.items
        guard !items.isEmpty else {
            output?.makeOrderDidFailWith(error: OrderError.cartIsEmpty)
            return
        }
        
        updater.makeOrder(items, success: { [weak self] in
            UserDefaults.cartObserver.value.removeAllProducts()
            self?.output?.orderSuccessfullyCreated()
        }) { [weak self ] error in
            self?.output?.makeOrderDidFailWith(error: error)
        }
    }
}
