//
//  CartInteractorIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol CartInteractorInput: class {
    
    func loadCartItems()
    
    func product(selected product: Product)
    func product(deselected product: Product)
    
    func makeOrder()
}

protocol CartInteractorOutput: class {
    
    func cartItemsLoaded(products: [Product])
    
    func orderSuccessfullyCreated()
    func makeOrderDidFailWith(error: Error)
}
