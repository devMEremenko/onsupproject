//
//  OrdersInteractor.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class OrdersInteractor {
    
    weak var output: OrdersInteractorOutput?
    fileprivate lazy var observer = OrderObserver(observers: [self])
}

extension OrdersInteractor: OrdersInteractorInput {
    
    func loadOrders() {
        observer.startObserving()
    }
}

extension OrdersInteractor: FirebaseObserver {
    
    typealias Model = Order
    
    func data(added models: [Order]) {
        output?.ordersAdded(orders: models)
    }
    
    func data(changed models: [Order]) {
        output?.ordersChanged(orders: models)
    }
    
    func data(removed models: [Order]) {
        output?.ordersRemoved(orders: models)
    }
}
