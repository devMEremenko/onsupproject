//
//  OrdersAssembly.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class OrdersAssembly {
    
    let wireframe = OrdersWireframe()
    
    private let vc = OrdersVC()
    private let presenter = OrdersPresenter()
    private let interactor = OrdersInteractor()
    
    init() {
        vc.eventHandler = presenter
        presenter.userInterface = vc
        presenter.wireframe = wireframe
        presenter.interactor = interactor

        wireframe.viewController = vc
        wireframe.moduleInterface = presenter

        interactor.output = presenter
    }
}
