//
//  OrderCell.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import UIKit
import DTModelStorage

class OrderCell: UITableViewCell, ModelTransfer {
    
    @IBOutlet weak var timeLabel: UILabel!
    @IBOutlet weak var countLabel: UILabel!
    @IBOutlet weak var statusLabel: UILabel!
    
    typealias ModelType = OrderCellViewModel
    
    func update(with model: OrderCellViewModel) {
        
        let order = model.order
        
        timeLabel.text = order.date?.timeAgo ?? ""
        countLabel.text = "Товаров: " + String(order.productIDs?.count ?? 0)
        statusLabel.text = order.status?.localizedTitle
    }
}

class OrderCellViewModel: Hashable {
    
    private(set) var order: Order
    
    init(_ order: Order) {
        self.order = order
    }
    
    var hashValue: Int {
        return order.hashValue
    }

    static func == (lhs: OrderCellViewModel, rhs: OrderCellViewModel) -> Bool {
        return lhs.order.id == rhs.order.id
    }
}
