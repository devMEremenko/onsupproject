//
//  OrdersVC.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class OrdersVC: UIViewController {

    var eventHandler: OrdersModuleInput?
    fileprivate let contentView = OrdersView()
    fileprivate lazy var controller = OrderController(table: contentView.table)
    
    override func loadView() {
        self.view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventHandler?.viewDidSetup()
        title = String.loc("orders.title")
    }
}

extension OrdersVC: OrdersUserInterfaceInput {
    
    func ordersAdded(orders: [Order]) {
        controller.add(orders)
    }
    
    func ordersChanged(orders: [Order]) {
        controller.update(orders)
    }
    
    func ordersRemoved(orders: [Order]) {
        controller.remove(orders)
    }
}
