//
//  OrdersView.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class OrdersView: UIView {
    
    let table = UITableView(frame: .zero, style: .plain)
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubview(table)
        table.rowHeight = 64.0
        table.snp.makeConstraints { maker in maker.edges.equalTo(self) }
    }
}
