//
//  OrdersUserInterfaceIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol OrdersUserInterfaceInput: class {
    
    func ordersAdded(orders: [Order])
    func ordersChanged(orders: [Order])
    func ordersRemoved(orders: [Order])
}

protocol OrdersUserInterfaceOutput: class {
    
}
