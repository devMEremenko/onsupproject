//
//  OrderController.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import UIKit
import DTTableViewManager

class OrderController: DTTableViewManageable {
    
    var tableView: UITableView!
    
    private var items = [Order]()
    
    init(table: UITableView) {
        tableView = table
        manager.startManaging(withDelegate: self)
        manager.register(OrderCell.self)
        manager.didSelect(OrderCell.self) { (_, _, path) in
            self.tableView.deselectRow(at: path, animated: true)
        }
    }
    
    func add(_ orders: [Order]) {
        
        items.append(contentsOf: orders)
        
        let viewModels = orders.map({ OrderCellViewModel($0) })
        manager.memoryStorage.addItems(viewModels)
    }
    
    func update(_ orders: [Order]) {
        
        guard let items = manager.memoryStorage.items(inSection: 0) as? [OrderCellViewModel] else {
            return
        }
        
        let updatedModels = orders.map({ OrderCellViewModel($0) })
        let intersection = Set<OrderCellViewModel>(updatedModels).intersection(items)
        manager.memoryStorage.removeItems(Array(intersection))
        manager.memoryStorage.addItems(updatedModels)
    }
    
    func remove(_ orders: [Order]) {
        guard let items = manager.memoryStorage.items(inSection: 0) as? [OrderCellViewModel] else {
            return
        }
        
        let updatedModels = orders.map({ OrderCellViewModel($0) })
        let intersection = Set<OrderCellViewModel>(updatedModels).intersection(items)
        manager.memoryStorage.removeItems(Array(intersection))
    }
}
