//
//  OrdersPresenter.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class OrdersPresenter: OrdersModuleInput {
    
    var interactor: OrdersInteractorInput?
    var wireframe: OrdersWireframe?
    weak var userInterface: OrdersUserInterfaceInput?
    weak var moduleOutput: OrdersModuleOutput?
    
    func viewDidSetup() {
        interactor?.loadOrders()
    }
}

extension OrdersPresenter: OrdersInteractorOutput {
    
    func ordersAdded(orders: [Order]) {
        userInterface?.ordersAdded(orders: orders)
    }
    
    func ordersChanged(orders: [Order]) {
        userInterface?.ordersChanged(orders: orders)
    }
    
    func ordersRemoved(orders: [Order]) {
        userInterface?.ordersRemoved(orders: orders)
    }
}
