//
//  OrdersModuleIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol OrdersModuleInput: BaseModuleInput {
    
}

protocol OrdersModuleOutput: class {
    
}
