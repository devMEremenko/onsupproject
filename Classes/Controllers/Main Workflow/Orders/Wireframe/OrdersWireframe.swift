//
//  OrdersWireframe.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class OrdersWireframe {
    
    weak var viewController: UIViewController?
    weak var moduleInterface: OrdersModuleInput?
}
