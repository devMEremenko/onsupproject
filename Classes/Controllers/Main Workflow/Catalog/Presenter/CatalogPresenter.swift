//
//  CatalogPresenter.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class CatalogPresenter {
    
    var interactor: CatalogInteractorInput?
    var wireframe: CatalogWireframe?
    weak var userInterface: CatalogUserInterfaceInput?
    weak var moduleOutput: CatalogModuleOutput?
}

extension CatalogPresenter: CatalogModuleInput {
    
    func viewDidSetup() {
        interactor?.loadCategories()
    }
    
    func presentSearch() {
        wireframe?.presentSearch()
    }
    
    func presentCart() {
        wireframe?.presentCart()
    }
    
    func presentProducts(for category: Category) {
        wireframe?.presentProducts(with: category)
    }
}

extension CatalogPresenter: CatalogInteractorOutput {
    
    func categories(changed categories: [Category]) {
        userInterface?.update(categories: categories)
    }
    
    func categories(removed categories: [Category]) {
        userInterface?.remove(categories: categories)
    }
    
    func categoriesLoaded(_ categories: [Category]) {
        userInterface?.add(categories: categories)
    }
    
    func loadCategoriesDidFail(error: Error) {
        userInterface?.show(error: error)
    }
    
    func presentProducts(with category: Category) {
        wireframe?.presentProducts(with: category)
    }
}
