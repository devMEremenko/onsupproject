//
//  CatalogModuleIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol CatalogModuleInput: BaseModuleInput {
    
    func presentSearch()
    func presentCart()
    
    func presentProducts(for category: Category)
}

protocol CatalogModuleOutput: class {
    
}
