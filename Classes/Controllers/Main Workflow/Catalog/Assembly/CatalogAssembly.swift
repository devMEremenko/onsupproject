//
//  CatalogAssembly.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class CatalogAssembly {
    
    let wireframe = CatalogWireframe()
    
    private let vc = CatalogVC()
    private let presenter = CatalogPresenter()
    private let interactor = CatalogInteractor()
    
    init() {
        vc.eventHandler = presenter
        presenter.userInterface = vc
        presenter.wireframe = wireframe
        presenter.interactor = interactor

        wireframe.viewController = vc
        wireframe.moduleInterface = presenter

        interactor.output = presenter
    }
}
