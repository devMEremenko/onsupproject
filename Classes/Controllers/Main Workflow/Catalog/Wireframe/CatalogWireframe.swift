//
//  CatalogWireframe.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import EasyCalls

class CatalogWireframe {
    
    weak var viewController: UIViewController?
    weak var moduleInterface: CatalogModuleInput?
}

extension CatalogWireframe {
    
    func presentSearch() {
        
    }
    
    func presentCart() {
        DispatchQueue.toMain {
            self.viewController?.safePush(vc: CartAssembly().vc)
        }
    }
    
    func presentProducts(with category: Category) {
        
        DispatchQueue.toMain {
            let assembly = ProductsAssembly()
            let moduleInterface = assembly.wireframe.moduleInterface
            moduleInterface?.setupForProductsPresentation(category: category)
            
            guard let vc = assembly.wireframe.viewController else { return }
            
            let nc = self.viewController?.navigationController
            nc?.pushViewController(vc, animated: true)
        }
    }
}


