//
//  CatalogInteractorIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol CatalogInteractorInput: class {
    
    func loadCategories()
}

protocol CatalogInteractorOutput: class {
    
    func categoriesLoaded(_ categories: [Category])
    func loadCategoriesDidFail(error: Error)
    
    func categories(changed: [Category])
    func categories(removed: [Category])
    
    //TODO: remove
    func presentProducts(with category: Category)
}
