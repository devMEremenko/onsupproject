//
//  CatalogInteractor.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class CatalogInteractor {
    
    weak var output: CatalogInteractorOutput?
    
    private lazy var observer = CategoryObserver(observers: [self])
}

extension CatalogInteractor: CatalogInteractorInput {
    
    func loadCategories() {
        observer.startObserving()
    }
}

extension CatalogInteractor: FirebaseObserver {
    
    typealias Model = Category
    
    func data(added models: [Category]) {
        output?.categoriesLoaded(models)
    }
    
    func data(changed models: [Category]) {
        output?.categories(changed: models)
    }
    
    func data(removed models: [Category]) {
        output?.categories(removed: models)
    }
}
