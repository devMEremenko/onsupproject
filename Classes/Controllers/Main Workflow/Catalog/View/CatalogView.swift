//
//  CatalogView.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class CatalogView: UIView {
    
    let collection: UICollectionView = {
        let layout = UICollectionViewFlowLayout()
        layout.minimumLineSpacing = 1
        layout.minimumInteritemSpacing = 0.5
        
        let windowSize = UIApplication.shared.keyWindow?.bounds.size ?? .zero
        let width = (windowSize.width / 2).floor - layout.minimumInteritemSpacing
        
        layout.itemSize = CGSize(width: width, height: 200)
        let collection = UICollectionView(frame: .zero, collectionViewLayout: layout)
        collection.accept(style: Theme.ScrollView.default)
        return collection
    }()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        backgroundColor = .white
        addSubview(collection)
        collection.snp.makeConstraints { maker in maker.edges.equalTo(self) }
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension CGFloat {
    
    var ceil: CGFloat {
        return CGFloat(ceilf(Float(self)))
    }
    
    var floor: CGFloat {
        return CGFloat(floorf(Float(self)))
    }
}
