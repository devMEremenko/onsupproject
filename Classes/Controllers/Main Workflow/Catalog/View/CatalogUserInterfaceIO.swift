//
//  CatalogUserInterfaceIO.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol CatalogUserInterfaceInput: BaseInterfaceInput {
    
    func add(categories: [Category])
    func update(categories: [Category])
    func remove(categories: [Category])
}

protocol CatalogUserInterfaceOutput: class {
    
}
