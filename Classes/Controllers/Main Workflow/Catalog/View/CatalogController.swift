//
//  CatalogController.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import DTModelStorage
import DTCollectionViewManager

class CatalogController: DTCollectionViewManageable {
    
    var collectionView: UICollectionView?
    private lazy var items = [Category]()
    
    init(_ collection: UICollectionView) {
        collectionView = collection
        
        manager.startManaging(withDelegate: self)
        manager.register(CatalogCell.self)
        manager.didSelect(CatalogCell.self) { [weak self] cell, model, _ in
            self?.categorySelection?(model.category)
        }
    }
    
    typealias CategorySelection = (Category) -> ()
    var categorySelection: CategorySelection?
    
    func update(categories: [Category]) {
        
        guard let items = manager.memoryStorage.items(inSection: 0) as? [CatalogCellViewModel] else {
            return
        }
        
        categories.forEach { item in
            guard let idx = items.index(where: { $0.category == categories.first }) else {
                return
            }
            try? manager.memoryStorage.replaceItem(items[idx],
                                                   with: CatalogCellViewModel(item))
        }
    }
    
    func add(categories: [Category]) {
        items.append(contentsOf: categories)
        
        let viewModels = categories.map({ CatalogCellViewModel($0)})
        manager.memoryStorage.addItems(viewModels)
    }
    
    func remove(categories: [Category]) {
        guard let items = manager.memoryStorage.items(inSection: 0) as? [CatalogCellViewModel] else {
            return
        }
        
        let updatedModels = categories.map({ CatalogCellViewModel($0) })
        let intersection = Set<CatalogCellViewModel>(updatedModels).intersection(items)
        manager.memoryStorage.removeItems(Array(intersection))
    }
}
