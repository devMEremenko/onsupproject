//
//  CatalogCell.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import DTModelStorage

class CatalogCell: UICollectionViewCell, ModelTransfer {
    
    typealias ModelType = CatalogCellViewModel
    
    @IBOutlet weak var imageView: UIImageView!
    @IBOutlet weak var nameLabel: UILabel!
    
    func update(with model: CatalogCellViewModel) {
        nameLabel.text = model.category.name
        imageView.updateImage(url: model.category.imageUrlString?.url)
    }
    
    override func prepareForReuse() {
        super.prepareForReuse()
        imageView.image = nil
    }
}

struct CatalogCellViewModel: Hashable {
    
    var hashValue: Int {
        return category.hashValue
    }
    
    static func == (lhs: CatalogCellViewModel, rhs: CatalogCellViewModel) -> Bool {
        return lhs.category == rhs.category
    }
    
    var category: Category
    
    init(_ category: Category) {
        self.category = category
    }
}
