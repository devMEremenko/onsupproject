//
//  CatalogVC.h
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

class CatalogVC: UIViewController {

    var eventHandler: CatalogModuleInput?
    fileprivate let contentView = CatalogView()
    fileprivate lazy var controller = CatalogController(contentView.collection)
    
    override func loadView() {
        self.view = contentView
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        eventHandler?.viewDidSetup()
        
        title = String.loc("catalog.title")
        
        navigationItem.rightBarButtonItem = UIBarButtonItem.cart {
            self.eventHandler?.presentCart()
        }
        
        controller.categorySelection = { [weak self] category in
            self?.eventHandler?.presentProducts(for: category)
        }
    }
}

extension CatalogVC: CatalogUserInterfaceInput {
    
    func add(categories: [Category]) {
        controller.add(categories: categories)
    }
    
    func remove(categories: [Category]) {
        controller.remove(categories: categories)
    }
    
    func update(categories: [Category]) {
        controller.update(categories: categories)
    }
}
