//
//  CartStorage.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

struct CartStorage {
    
    private(set) var items = [Product]()
    
    mutating func add(item: Product) {
        self.items.append(item)
    }
    
    mutating func remove(item: Product) {
        self.items.remove(item: item)
    }
    
    mutating func removeAllProducts() {
        self.items.removeAll()
    }
}

extension CartStorage {
    
    func count(of product: Product) -> Int {
        
        var map = [Product: Int]()
        
        items.forEach { item in
            if let key = map[item] {
                map[item] = key + 1
            } else {
                map[item] = 1
            }
        }
        return map[product] ?? 0
    }
}

extension Array where Element == Product {
    
    func index(of item: Product) -> Int? {
        return index(where: { $0 == item })
    }
    
    mutating func remove(item: Product) {
        guard let idx = self.index(of: item) else { return }
        self.remove(at: idx)
    }
}
