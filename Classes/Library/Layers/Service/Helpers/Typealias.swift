//
//  Helpers.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

public typealias Empty = () -> ()
public typealias ErrorBlock = (Error) -> ()
