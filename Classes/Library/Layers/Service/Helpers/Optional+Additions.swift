//
//  Optional+Additions.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

extension Optional {
    
    var isExist: Bool {
        return self != nil
    }
    
    var isNil: Bool {
        return self == nil
    }
}
