//
//  Enum+Extension.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation

extension RawRepresentable {
    
    var raw: Self.RawValue {
        return rawValue
    }
}
