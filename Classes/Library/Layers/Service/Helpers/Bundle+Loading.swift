//
//  Bundle+Loading.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

extension Bundle {
    
    static func loadView<V>(type: V.Type) -> V? {
        let view = Bundle.main.loadNibNamed(String(describing: V.self),
                                            owner: nil,
                                            options: nil)?.first
        return view as? V
    }
}

