//
//  Errors.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation
import Moya

enum AppError: Errorable {
    case general
    case mapping
}

enum OrderError: Errorable {
    case cartIsEmpty
    
    var message: String {
        switch self {
        case .cartIsEmpty:
            return String.loc("error.order.is.empty")
        }
    }
}

enum ZipError: Errorable {
    case codeIsEmpty
    case codeInvalid
    
    var message: String {
        switch self {
        case .codeIsEmpty:
            return String.loc("error.zip.code.is.empty")
        case .codeInvalid:
            return String.loc("error.zip.code.invalid")
        }
    }
}

enum CatalogError: Errorable {
    
    case zipIsNotProvided
    
    var message: String {
        switch self {
        case .zipIsNotProvided:
            return String.loc("error.catalog.zip.code.invalid")
        }
    }
}

enum AuthError: Errorable {
    case emailIsEmpty
    case passIsEmpty
    case passwordsAreDifferent
}

struct MappedError: Errorable {
    
    private let _message: String
    
    init(message: String) {
        _message = message
    }
    
    var message: String { return _message }
}

protocol Errorable: LocalizedError {
    
    var message: String { get }
}

extension Errorable {
    
    var message: String {
        return String.loc("error.general.message")
    }
}

extension Error {
    
    var tryLocalize: String? {
        guard let error = self as? Errorable else {
            return self.networkErrorMessage() ?? String.loc("error.general.message")
        }
        return error.networkErrorMessage() ?? error.message
    }
    
    func networkErrorMessage() -> String? {
        let error = (isMoyaError ?? self) as NSError
        switch error.code {
            case -1001: return String.loc("error.network.timeout.message")
            case -1005: return String.loc("error.network.connection.lost.message")
            case -1009: return String.loc("error.network.not.connected.message")
        default: return nil
        }
    }
}

extension Errorable where Self == AppError {
    
    var message: String {
        switch self {
        case .general:
            return String.loc("error.general.message")
        case .mapping:
            return String.loc("error.general.message")
        }
    }
}

extension Errorable where Self == AuthError {
    
    var message: String {
        switch self {
        case .emailIsEmpty:
            return String.loc("error.email.is.empty.message")
        case .passIsEmpty:
            return String.loc("error.pass.is.empty.message")
        case .passwordsAreDifferent:
            return String.loc("error.password.are.different.message")
        }
    }
}

extension Error {
    
    var isMoyaError: Error? {
        guard let error = self as? MoyaError else {
            return nil
        }
        switch error {
        case let .underlying(error, _):
            return error
        default: return nil
        }
    }
}
