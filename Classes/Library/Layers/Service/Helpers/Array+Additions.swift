//
//  Array+Additions.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

extension Array {
    
    func next(after index: Int) -> Element? {
        let next = index + 1
        guard next >= 0, next < count, count != 0 else { return nil }
        return self[next]
    }
    
    func obj(at index: Int) -> Element? {
        guard index >= 0 && index < count else { return nil }
        return self[index]
    }
}
