//
//  Defaults+Access.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation
import DefaultsKit
import Observable

extension UserDefaults {
    
    static func addCart(item: Product) {
        var storage = cartObserver.value
        storage.add(item: item)
        cartObserver.value = storage
    }
    
    static func removeCart(item: Product) {
        var storage = cartObserver.value
        storage.remove(item: item)
        cartObserver.value = storage
    }
    
    static var cartObserver: Observable<CartStorage> = {
        return Observable(CartStorage())
    }()
        
    static func resetAllData() {
        Defaults.shared.clear(Key.with(User.self))
    }
    
    static var isSignedIn: Bool {
        if standard.bool(forKey: "isSignedIn") {
            return true
        } else {
            standard.set(true, forKey: "isSignedIn")
            return false
        }
    }
}

fileprivate extension UserDefaults {
    
    static func retrieve<T: Codable>(type: T.Type) -> T? {
        return Defaults.shared.get(for: Key.with(T.self))
    }
    
    static func update<T: Codable>(model: T) {
        Defaults.shared.set(model, for: Key.with(T.self))
    }
}

fileprivate extension Key {
    
    static func with(_ type: ValueType.Type) -> Key<ValueType> {
        return Key<ValueType>(String(describing: ValueType.self))
    }
}
