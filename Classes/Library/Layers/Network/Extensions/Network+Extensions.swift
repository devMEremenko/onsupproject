//
//  Network+Extensions.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation
import Moya

extension Optional where Wrapped == MoyaError {
    
    var isExist: Bool {
        return self != nil
    }
    
    var isNil: Bool {
        return self == nil
    }
}
