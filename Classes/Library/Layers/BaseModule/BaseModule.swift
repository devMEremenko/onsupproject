//
//  BaseModule.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import EasyCalls

enum Route {
    case entrance
    case tabBar
}

extension Route {
    
    var viewController: UIViewController {
        switch self {
        case .entrance:
            let assembly = ProfileAssembly()
            assembly.wireframe.moduleInterface?.performEntranceFlow()
            return assembly.vc
        case .tabBar:
            return TabBarWireframe().tabBar
        }
    }
}

protocol BaseModuleInput: class {
    
    func viewDidSetup()
}

protocol BaseInterfaceInput: class {
    
    func dismiss()
    func dismiss(_ completion: Empty?)
    func show(error: Error)
    func show(message: String)
}

extension BaseInterfaceInput where Self: UIViewController {
    
    func dismiss() {
        dismiss(nil)
    }
    
    func dismiss(_ completion: Empty? = nil) {
        safeDismiss(completion: completion)
    }
    
    func show(error: Error) {
        let title = String.loc("error.general.title")
        show(title: title, message: error.tryLocalize, actions: Action.ok)
    }
    
    func show(message: String) {
        show(title: nil, message: message, actions: Action.ok)
    }
}
