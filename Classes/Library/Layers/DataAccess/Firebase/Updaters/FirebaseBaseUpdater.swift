//
//  FirebaseBaseUpdater.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation
import FirebaseDatabase

protocol Updater {
    
}

class FirebaseBaseUpdater {
    
    private var query: DatabaseReference
    
    init(query: DatabaseReference) {
        self.query = query
    }
    
    func update(query: DatabaseReference) {
        self.query = query
    }
    
    func send(value: [String : Any?],
              _ success: Empty? = nil,
              _ failure: ErrorBlock? = nil) {
        
        self.query.setValue(value) { error, ref in
            guard let error = error else {
                success?(); return
            }
            failure?(error)
        }
    }
    
    static var baseReference: DatabaseReference {
        return Database.database().reference()
    }
}
