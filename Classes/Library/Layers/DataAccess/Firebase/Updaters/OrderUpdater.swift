//
//  OrderUpdater.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class OrderUpdater: FirebaseBaseUpdater {
    
    init() {
        super.init(query: OrderUpdater.ordersReference)
    }
    
    func makeOrder(_ products: [Product],
                   success: Empty? = nil,
                   _ failure: ErrorBlock? = nil) {
        
        let ids = products.compactMap({ $0.id })
        
        let date = Date().timeIntervalSince1970
        let id = Auth.auth().currentUser?.uid
        let value: [String : Any] = [Order.Keys.productIDs.raw : ids,
                                     Order.Keys.status.raw : OrderStatus.inQuery.raw,
                                     Order.Keys.date.raw : date,
                                     Order.Keys.userID.raw : id as Any]
        
        update(query: OrderUpdater.ordersReference.childByAutoId())
        send(value: value, success, failure)
    }
}

extension OrderUpdater {
    
    static var ordersReference: DatabaseReference {
        return FirebaseBaseUpdater.baseReference.child(Path.Root.orders.raw)
    }
}
