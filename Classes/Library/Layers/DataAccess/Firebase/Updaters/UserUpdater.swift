//
//  UserUpdater.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class UserUpdater: FirebaseBaseUpdater {
    
    init() {
        super.init(query: UserUpdater.reference)
    }
    
    func update(_ user: User,
                success: Empty? = nil,
                _ failure: ErrorBlock? = nil) {
        
        Auth.auth().signInAnonymously { firUser, error in
            guard error.isNil, let firUser = firUser else {
                return
            }
            
            let value = [User.Keys.firstName.raw : user.firstName,
                         User.Keys.lastName.raw : user.lastName,
                         User.Keys.address.raw : user.address,
                         User.Keys.phone.raw : user.phone]
            
            self.update(query: UserUpdater.reference
                .child(firUser.uid)
                .child(User.Keys.info.raw))
            
            self.send(value: value, success, failure)
        }
    }
}

extension UserUpdater {
    
    static var reference: DatabaseReference {
        return FirebaseBaseUpdater.baseReference.child(Path.Root.users.raw)
    }
}
