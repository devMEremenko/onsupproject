//
//  FirebaseManager.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation
import FirebaseDatabase

protocol FirebaseObserver {
    
    associatedtype Model: DomainModel
    
    func data(added models: [Model])
    func data(changed models: [Model])
    func data(removed models: [Model])
}

class BaseReferenceObserver<Observer: FirebaseObserver> {
    
    private var observers: [Observer]
    private var query: DatabaseQuery
    
    init(query: DatabaseQuery, observers: [Observer] = []) {
        self.query = query
        self.observers = observers
    }
    
    func startObserving() {
        query.observe(.childAdded) { [weak self] snapshot in
            self?.notifyObservers(snapshot) {
                $0.data(added: [$1])
            }
        }
        query.observe(.childChanged) { [weak self] snapshot in
            self?.notifyObservers(snapshot) {
                $0.data(changed: [$1])
            }
        }
        query.observe(.childRemoved) { [weak self] snapshot in
            self?.notifyObservers(snapshot) {
                $0.data(removed: [$1])
            }
        }
    }
    
    func domainModel(from reference: DataSnapshot) -> Observer.Model? {
        fatalError("Should be overridden in subclasses")
    }
}

extension BaseReferenceObserver {
    
    static var baseReference: DatabaseReference {
        return Database.database().reference()
    }
    
    private func notifyObservers(_ snapshot: DataSnapshot,
                                 block: @escaping (Observer, Observer.Model) -> Void) {
        guard let model = domainModel(from: snapshot) else { return }
        DispatchQueue.toMain {
            self.observers.forEach { block($0, model) }
        }
    }
}
