//
//  OrderObserver.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation
import FirebaseDatabase

class OrderObserver<Observer: FirebaseObserver>: BaseReferenceObserver<Observer> {
    
    init(observers: [Observer] = []) {
        
        let ref = type(of: self).baseReference.child(Path.Root.orders.raw)
        super.init(query: ref, observers: observers)
    }
    
    override func domainModel(from reference: DataSnapshot) -> Observer.Model? {
        return (FIRModelsParser().parse(snapshot: reference) as Order?) as? Observer.Model
    }
}
