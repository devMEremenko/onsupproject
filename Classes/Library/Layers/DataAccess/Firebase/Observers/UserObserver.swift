//
//  UserObserver.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation
import FirebaseAuth
import FirebaseDatabase

class UserObserver<Observer: FirebaseObserver>: BaseReferenceObserver<Observer> {
    
    init(observers: [Observer] = []) {
        
        let userID = Auth.auth().currentUser?.uid ?? "-1"
        let ref = type(of: self).baseReference
            .child(Path.Root.users.raw)
            .child(userID)
        super.init(query: ref, observers: observers)
    }
    
    override func domainModel(from reference: DataSnapshot) -> Observer.Model? {
        return (FIRModelsParser().parse(snapshot: reference) as User?) as? Observer.Model
    }
}
