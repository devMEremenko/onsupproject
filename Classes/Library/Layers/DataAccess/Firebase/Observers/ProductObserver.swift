//
//  ProductObserver.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation
import FirebaseDatabase

class ProductObserver<Observer: FirebaseObserver>: BaseReferenceObserver<Observer> {
    
    init(observers: [Observer] = [], categoryID: String) {
        
        let ref = type(of: self).baseReference
            .child(Path.Root.products.rawValue)
            .queryOrdered(byChild: Path.Product.categoryID.raw)
            .queryEqual(toValue: categoryID)
        
        super.init(query: ref, observers: observers)
    }
    
    override func domainModel(from reference: DataSnapshot) -> Observer.Model? {
        return (FIRModelsParser().parse(snapshot: reference) as Product?) as? Observer.Model
    }
}
