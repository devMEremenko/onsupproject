//
//  FIRModelsParser.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation
import FirebaseDatabase

class FIRModelsParser {
        
    func parse(snapshot: DataSnapshot) -> Category? {
        
        guard let items = snapshot.value as? NSDictionary else { return nil }
        
        let url = items[Category.Keys.imageUrl.rawValue] as? String
        let name = items[Category.Keys.name.rawValue] as? String
        
        return Category(id: snapshot.key,
                        name: name,
                        imageUrlString: url)
    }
    
    func parse(snapshot: DataSnapshot) -> Product? {
        
        guard let items = snapshot.value as? NSDictionary else { return nil }
        
        let categoryId = items[Product.Keys.categoryId.raw] as? String
        let price = items[Product.Keys.price.raw] as? Double
        let name = items[Product.Keys.name.raw] as? String
        let unit = items[Product.Keys.unit.raw] as? String
        let url = items[Product.Keys.image.raw] as? String
        
        return Product(id: snapshot.key,
                       categoryId: categoryId,
                       price: price,
                       name: name,
                       unit: unit,
                       imageUrlString: url)
    }
    
    func parse(snapshot: DataSnapshot) -> Order? {
        
        guard let items = snapshot.value as? NSDictionary else { return nil }
        
        let order = Order()
        order.id = snapshot.key
        order.productIDs = items[Order.Keys.productIDs.raw] as? [String]
        
        if let status = items[Order.Keys.status.raw] as? String {
            order.status = OrderStatus(rawValue: status)
        }
        
        if let date = items[Order.Keys.date.raw] as? TimeInterval {
            order.date = Date(timeIntervalSince1970: date)
        }
        
        return order
    }
    
    func parse(snapshot: DataSnapshot) -> User? {
        
        guard let items = snapshot.value as? NSDictionary else { return nil }
        
        let user = User()
        user.id = snapshot.key
        user.firstName = items[User.Keys.firstName.raw] as? String
        user.lastName = items[User.Keys.lastName.raw] as? String
        user.address = items[User.Keys.address.raw] as? String
        user.phone = items[User.Keys.phone.raw] as? String
        
        return user
    }
}
