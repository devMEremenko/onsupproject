//
//  Paths.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation

struct Path {
    
    enum Root: String {
        case categories = "categories"
        case products = "products"
        case orders = "orders"
        case users = "users"
    }
    
    enum Product: String {
        case categoryID = "categoryID"
    }
}
