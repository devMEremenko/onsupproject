//
//  Base.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

protocol DomainModel: class {
    
    var id: String? { get set }
}

extension Equatable where Self: DomainModel {
    
}

func == (lhs: DomainModel, rhs: DomainModel) -> Bool {
    return lhs.id == rhs.id && lhs === rhs
}
