//
//  User.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class User: DomainModel, Codable {
    var id: String?
    var firstName: String?
    var lastName: String?
    var address: String?
    var phone: String?
    
    enum Keys: String, CodingKey {
        case info = "info"
        case id = "id"
        case firstName = "first_name"
        case lastName = "last_name"
        case address = "address"
        case phone = "phone"
    }
    
    init(id: String? = nil,
         firstName: String? = nil,
         lastName: String? = nil,
         address: String? = nil,
         phone: String? = nil) {
        
        self.id = id
        self.firstName = firstName
        self.lastName = lastName
        self.address = address
        self.phone = phone
    }
}
