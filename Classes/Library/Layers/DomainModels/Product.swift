//
//  Product.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class Product: DomainModel, Codable, Hashable {
    
    var id: String?
    var categoryId: String?
    var price: Double?
    var name: String?
    var unit: String?
    var imageUrlString: String?
    
    enum Keys: String, CodingKey {
        case id = "id"
        case categoryId = "categoryId"
        case price = "price"
        case name = "name"
        case unit = "unit"
        case image = "image"
    }
    
    init() {}
    
    init(id: String?,
         categoryId: String?,
         price: Double?,
         name: String?,
         unit: String?,
         imageUrlString: String?) {
        self.id = id
        self.categoryId = categoryId
        self.price = price
        self.name = name
        self.unit = unit
        self.imageUrlString = imageUrlString
    }
    
    static func ==(lhs: Product, rhs: Product) -> Bool {
        return lhs.id == rhs.id
    }
    
    var hashValue: Int {
        return (id ?? "0").hashValue
    }
}
