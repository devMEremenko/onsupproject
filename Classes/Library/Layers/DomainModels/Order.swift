//
//  Order.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

enum OrderStatus: String {
    case inQuery = "in-query"
    case inProgress = "in-progress"
    case completed = "completed"
    
    var localizedTitle: String {
        switch self {
        case .inQuery:
            return String.loc("order.status.in.query")
        case .inProgress:
            return String.loc("order.status.in.progress")
        case .completed:
            return String.loc("order.status.in.completed")
        }
    }
}

class Order: DomainModel {
    
    var id: String?
    var productIDs: [String]?
    var status: OrderStatus?
    var date: Date?
    
    init() {}
    
    enum Keys: String, CodingKey {
        case id = "id"
        case productIDs = "products"
        case status = "status"
        case date = "date"
        case userID = "userID"
    }
}

extension Order: Hashable {
    
    static func == (lhs: Order, rhs: Order) -> Bool {
        return lhs.id == rhs.id
    }
    
    var hashValue: Int {
        return id?.hashValue ?? 0
    }
}
