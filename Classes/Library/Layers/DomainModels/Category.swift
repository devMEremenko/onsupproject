//
//  Category.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

class Category: DomainModel, Codable, Hashable {
    
    static func == (lhs: Category, rhs: Category) -> Bool {
        return lhs.id == rhs.id
    }
    
    var hashValue: Int {
        return (id ?? "").hashValue
            ^ (name ?? "").hashValue
            ^ (imageUrlString ?? "").hashValue
    }
    
    var id: String?
    var name: String?
    var imageUrlString: String?
    
    init(id: String?, name: String?, imageUrlString: String?) {
        self.id = id
        self.name = name
        self.imageUrlString = imageUrlString
    }
    
    enum Keys: String, CodingKey {
        case id = "id"
        case name = "name"
        case imageUrl = "image"
    }
}
