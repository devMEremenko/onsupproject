//
//  UICell+Additions.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

extension UITableViewHeaderFooterView {
    
    public static var reuseIdentifier: String {
        return String(describing: self)
    }
}
