//
//  Date+Additions.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 CustomerRPD. All rights reserved.
//

import Foundation

extension Date {
    
    var timeAgo: String {
        
        let interval = Calendar.current.dateComponents([.year, .month, .day, .hour, .minute, .second], from: self, to: Date())
        
        if let year = interval.year, year > 0 {
            return year == 1 ? "\(year)" + " " + "год назад" :
                "\(year)" + " " + "лет назад"
        } else if let month = interval.month, month > 0 {
            return month == 1 ? "\(month)" + " " + "месяц назад" :
                "\(month)" + " " + "месяцев назад"
        } else if let day = interval.day, day > 0 {
            return day == 1 ? "\(day)" + " " + "день назад" :
                "\(day)" + " " + "дней назад"
        } else if let hour = interval.hour, hour > 0 {
            return hour == 1 ? "\(hour)" + " " + "час назад" :
                "\(hour)" + " " + "часов назад"
        } else if let minute = interval.minute, minute > 0 {
            return minute == 1 ? "\(minute)" + " " + "минуту назад" :
                "\(minute)" + " " + "минут назад"
        } else if let second = interval.second, second > 0 {
            return second == 1 ? "\(second)" + " " + "секунду назад" :
                "\(second)" + " " + "секунд назад"
        } else {
            return "только что"
        }
    }
}
