//
//  UIViewController+Presentation.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

extension UIViewController {
    
    func safePresent(vc: UIViewController?,
                     animated: Bool = true,
                     completion: Empty? = nil) {
        
        DispatchQueue.toMain {
            guard let viewController = vc else { return }
            self.present(viewController, animated: animated, completion: completion)
        }
    }
    
    func safeDismiss(animated: Bool = true, completion: Empty? = nil) {
        DispatchQueue.toMain {
            if self.presentingViewController.isExist {
                self.dismiss(animated: animated, completion: completion)
            } else {
                self.navigationController?.popViewController(animated: animated)
                completion?()
            }
        }
    }
    
    func safePush(vc: UIViewController?,
                  animated: Bool = true,
                  completion: Empty? = nil) {
        
        guard let unVC = vc else { return }
        
        DispatchQueue.toMain {
            self.navigationController?.pushViewController(unVC, animated: animated)
        }
    }
    
    static var topVC: UIViewController? {
        return UIApplication.shared.keyWindow?.rootViewController
    }
}
