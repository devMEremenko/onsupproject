//
//  UITabBarItem+Additions.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

extension UITabBarItem {
    
    static var catalog: UITabBarItem {
        let title = String.loc("tab.bar.item.title.catalog")
        let image = UIImage(named: "bar_icon_catalog")?.resize(20)
        return UITabBarItem.with(title, image: image)
    }
    
    static var profile: UITabBarItem {
        let title = String.loc("tab.bar.item.title.profile")
        let image = UIImage(named: "bar_icon_profile")?.resize(20)
        return UITabBarItem.with(title, image: image)
    }
    
    static var orders: UITabBarItem {
        let title = String.loc("tab.bar.item.title.orders")
        let image = UIImage(named: "bar_icon_orders")?.resize(20)
        return UITabBarItem.with(title, image: image)
    }
    
    static func with(_ title: String? = nil, image: UIImage? = nil, selectedImage: UIImage? = nil) -> UITabBarItem {
        
        return UITabBarItem(title: title,
                            image: image,
                            selectedImage: selectedImage)
    }
}
