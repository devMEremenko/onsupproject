//
//  Handable.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

protocol Handable: class {
    
    var handler: Empty? { get set }
}

private struct AssociatedKeys {
    static var actionWrapper = "actionWrapper"
}

extension Handable {
    
    var handler: Empty? {
        get {
            let object = objc_getAssociatedObject(self, &AssociatedKeys.actionWrapper)
            guard let wrapper = object as? ClosureWrapper else { return nil }
            return wrapper.closure
        }
        set {
            let policy = objc_AssociationPolicy.OBJC_ASSOCIATION_RETAIN_NONATOMIC
            if let closure = newValue {
                let wrapper = ClosureWrapper(closure: closure)
                objc_setAssociatedObject(self, &AssociatedKeys.actionWrapper, wrapper, policy)
            } else {
                objc_setAssociatedObject(self, &AssociatedKeys.actionWrapper, nil, policy)
            }
        }
    }
}

