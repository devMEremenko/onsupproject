//
//  UIBarButtonItem+Helpers.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

extension UIBarButtonItem: Handable {
        
    public convenience init(style: UIBarButtonSystemItem,
                            _ handler: Empty?) {
        
        self.init(barButtonSystemItem: style, target: nil, action: nil)
        self.target = self
        self.action = #selector(UIBarButtonItem.actionSelected)
        self.handler = handler
    }
    
    public convenience init(title: String,
                            _ handler: Empty?) {
        
        self.init(title: title, style: .plain, target: nil, action: nil)
        self.target = self
        self.action = #selector(UIBarButtonItem.actionSelected)
        self.handler = handler
    }
    
    public convenience init(image: UIImage?,
                            _ handler: Empty?) {
        
        self.init(image: image, style: .plain, target: nil, action: nil)
        self.target = self
        self.action = #selector(UIBarButtonItem.actionSelected)
        self.handler = handler
    }
    
    @objc private func actionSelected() {
        handler?()
    }
}

extension UIBarButtonItem {
    
    static func cart(_ handler: Empty?) -> UIBarButtonItem {
        let image = UIImage(named: "bar_icon_cart")?.resize(26)
        return UIBarButtonItem(image: image, handler)
    }
}
