//
//  UIImageView+Networking.swift
//
//  Created by Eremenko Maxim on 12/7/17.
//  Copyright © 2017 Eremenko Maxim. All rights reserved.
//

import UIKit
import PINRemoteImage

extension UIImageView {
        
    func updateImage(url: URL?, placeholder: UIImage? = nil) {
        pin_setImage(from: url, placeholderImage: placeholder)
    }
}

extension String {
    
    var url: URL? {
        return URL(string: self)
    }
}
