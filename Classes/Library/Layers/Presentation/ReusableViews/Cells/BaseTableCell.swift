//
//  BaseTableCell.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import SnapKit

class BaseTableCell: UITableViewCell {
    
    enum SeparatorType {
        case top
        case bottom
    }
    
    lazy private(set) var topSeparator = UIView()
    lazy private(set) var bottomSeparator = UIView()
    
    override init(style: UITableViewCellStyle, reuseIdentifier: String?) {
        super.init(style: style, reuseIdentifier: reuseIdentifier)
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}

extension BaseTableCell {
    
    func enable(separator types: SeparatorType...) {
        types.forEach { type in
            switch type {
            case .top:
                enable(separator: topSeparator, type: type)
            case .bottom:
                enable(separator: bottomSeparator, type: type)
            }
        }
        self.accept(style: Theme.Cell.default)
    }
    
    func disable(separator: SeparatorType) {
        switch separator {
        case .top:
            disable(separator: topSeparator)
        case .bottom:
            disable(separator: bottomSeparator)
        }
    }
    
    private func enable(separator: UIView, type: SeparatorType) {
        guard separator.superview == nil else { return }
        contentView.addSubview(separator)
        separator.snp.makeConstraints { maker in
            maker.height.equalTo(0.5)
            maker.left.right.equalTo(self.contentView)
            switch type {
            case .top:
                maker.top.equalTo(self.contentView)
            case .bottom:
                maker.bottom.equalTo(self.contentView)
            }
        }
    }
    
    private func disable(separator: UIView) {
        guard separator.superview != nil else { return }
        separator.removeFromSuperview()
    }
}
