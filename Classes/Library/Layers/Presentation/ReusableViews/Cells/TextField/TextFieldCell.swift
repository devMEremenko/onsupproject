//
//  TextFieldTableCell.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit
import DTModelStorage

class TextFieldCell: BaseTableCell, ModelTransfer {
    
    typealias ModelType = TextFieldCellViewModel
    
    @IBOutlet weak var textField: UITextField!
    
    var model: ModelType?
    
    func update(with model: TextFieldCellViewModel) {
        selectionStyle = .none
        
        self.model = model
        self.model?.textField = textField
        
        textField.delegate = self
        textField.text = model.text
        textField.placeholder = model.placeholder
        textField.accept(style: model.style)
        enable(separator: .top, .bottom)
    }
}

extension TextFieldCell: UITextFieldDelegate {
    
    func textField(_ textField: UITextField,
                   shouldChangeCharactersIn range: NSRange,
                   replacementString string: String) -> Bool {
        self.model?.text = textField.text ?? "" + string
        return true
    }
    
    func textFieldShouldReturn(_ textField: UITextField) -> Bool {
        return model?.shouldReturnHandler?() ?? true
    }
    
    func textFieldDidEndEditing(_ textField: UITextField,
                                reason: UITextFieldDidEndEditingReason) {
        self.model?.text = textField.text
        self.model?.didEndEditingHandler?()
    }
}

class TextFieldCellViewModel {
    
    typealias Dismissal = Empty
    typealias ShouldReturn = () -> (Bool)
    
    var text: String?
    let placeholder: String?
    let style: TextFieldStyle
    var shouldReturnHandler: ShouldReturn?
    var didEndEditingHandler: Empty?
    
    fileprivate weak var textField: UITextField?
    
    init(placeholder: String, style: TextFieldStyle = Theme.TextField.default) {
        self.placeholder = placeholder
        self.style = style
    }
}

extension TextFieldCellViewModel {
    
    func activateField() {
        textField?.becomeFirstResponder()
    }
    
    func dismissField() {
        textField?.resignFirstResponder()
    }
}
