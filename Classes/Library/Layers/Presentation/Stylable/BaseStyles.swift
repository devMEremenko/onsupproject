//
//  Stylable.swift
//  OnSup
//
//  Created by Maxim Eremenko  .
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

protocol Style {
    
    var backgroundColor: UIColor { get }
}

protocol ButtonStyle: Style {
    
    var titleColor: UIColor { get }
}

protocol TextFieldStyle: Style {
    
    var textColor: UIColor { get }
    var isSecure: Bool { get }
    var keyboardType: UIKeyboardType { get }
    var autocorrection: UITextAutocorrectionType { get }
    var appearance: UIKeyboardAppearance { get }
    var returnKey: UIReturnKeyType { get }
}

protocol LabelStyle {
    
    var textColor: UIColor { get }
}

protocol ScrollViewStyle: Style {}

protocol TableStyle: ScrollViewStyle {
    
    var separator: UITableViewCellSeparatorStyle { get }
}

protocol CellStyle: Style {
    
    var topSeparatorColor: UIColor { get }
    var bottomSeparatorColor: UIColor { get }
}


//MARK: Extensions

extension Style {
    
    var backgroundColor: UIColor { return .clear }
}

extension ButtonStyle {
    
    var titleColor: UIColor { return .blue }
}

extension TextFieldStyle {
    
    var textColor: UIColor { return .black }
    var isSecure: Bool { return false }
    var keyboardType: UIKeyboardType { return .default }
    var autocorrection: UITextAutocorrectionType { return .default }
    var appearance: UIKeyboardAppearance { return .default }
    var returnKey: UIReturnKeyType { return .next }
}

extension TableStyle {
    
    var separator: UITableViewCellSeparatorStyle { return .none }
}
