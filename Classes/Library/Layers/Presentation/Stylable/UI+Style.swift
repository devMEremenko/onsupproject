//
//  UI+StyleExtensions.swift
//  OnSup
//
//  Created by Maxim Eremenko  .
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

extension UIView {
    
    convenience init(style: Style) {
        self.init()
        backgroundColor = style.backgroundColor
    }
    
    func accept(style: Style) {
        backgroundColor = style.backgroundColor
    }
}

extension UIButton {
    
    convenience init(style: ButtonStyle) {
        self.init()
        backgroundColor = style.backgroundColor
        setTitleColor(style.titleColor, for: .normal)
    }

    func accept(style: ButtonStyle) {
        backgroundColor = style.backgroundColor
        setTitleColor(style.titleColor, for: .normal)
    }
}

extension UIScrollView {
    
    convenience init(style: ScrollViewStyle) {
        self.init()
        backgroundColor = style.backgroundColor
    }
    
    func accept(style: ScrollViewStyle) {
        backgroundColor = style.backgroundColor
    }
}

extension UITableView {
    
    func accept(style: TableStyle) {
        backgroundColor = style.backgroundColor
        separatorStyle = style.separator
    }
}

extension UITextField {
    
    func accept(style: TextFieldStyle) {
        backgroundColor = style.backgroundColor
        textColor = style.textColor
        isSecureTextEntry = style.isSecure
        keyboardType = style.keyboardType
        autocorrectionType = style.autocorrection
        keyboardAppearance = style.appearance
        returnKeyType = style.returnKey
    }
}

extension BaseTableCell {
    
    func accept(style: OnSup.CellStyle) {
        backgroundColor = style.backgroundColor
        topSeparator.backgroundColor = style.topSeparatorColor
        bottomSeparator.backgroundColor = style.bottomSeparatorColor
    }
}
