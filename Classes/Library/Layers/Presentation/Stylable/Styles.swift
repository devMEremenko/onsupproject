//
//  Styles.swift
//  OnSup
//
//  Created by Maxim Eremenko  .
//  Copyright © 2019 OnSup. All rights reserved.
//

import UIKit

struct Theme: ButtonStyle {
    
    struct Buttons {
        static var `default`: ButtonStyle { return DefaultButtonStyle() }
    }
    
    struct TextField {
        static var `default`: TextFieldStyle { return DefaultTextFieldStyle() }
        static var secure: TextFieldStyle { return SecureTextFieldStyle() }
        static var email: TextFieldStyle { return EmailTextFieldStyle() }
        static var phone: TextFieldStyle { return PhoneTextFieldStyle() }
    }
    
    struct ScrollView {
        static var `default`: ScrollViewStyle { return DefaultScrollViewStyle() }
    }
    
    struct Table {
        static var `default`: TableStyle { return DefaultTableStyle() }
    }
    
    struct Cell {
        static var `default`: CellStyle { return DefaultCellStyle() }
    }
}

struct DefaultButtonStyle: ButtonStyle {
    
    var backgroundColor: UIColor { return UIColor(white: 0.9, alpha: 1)}
}

struct DefaultTextFieldStyle: TextFieldStyle {}
struct DefaultScrollViewStyle: ScrollViewStyle {
    
    var backgroundColor: UIColor { return UIColor(white: 0.95, alpha: 1)}
}

struct DefaultCellStyle: CellStyle {
    
    var backgroundColor: UIColor { return .white }
    var topSeparatorColor: UIColor { return UIColor(white: 0, alpha: 0.1) }
    var bottomSeparatorColor: UIColor { return UIColor(white: 0, alpha: 0.1) }
}

//MARK: - TextField
struct SecureTextFieldStyle: TextFieldStyle {
    var isSecure: Bool { return true }
    var autocorrection: UITextAutocorrectionType { return .no }
}

struct EmailTextFieldStyle: TextFieldStyle {
    var autocorrection: UITextAutocorrectionType { return .no }
    var keyboardType: UIKeyboardType { return .emailAddress }
}

struct PhoneTextFieldStyle: TextFieldStyle {
    var autocorrection: UITextAutocorrectionType { return .no }
    var keyboardType: UIKeyboardType { return .phonePad }
}

struct DefaultTableStyle: TableStyle {}
