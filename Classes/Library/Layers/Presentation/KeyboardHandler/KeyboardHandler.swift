//
//  KeyboardHandler.swift
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 Maxim Eremenko. All rights reserved.
//

import UIKit

open class KeyboardHandler: NSObject {
    
    deinit {
        let center = NotificationCenter.default
        
        center.removeObserver(self,
                              name: NSNotification.Name.UIKeyboardWillShow,
                              object: nil)
        
        center.removeObserver(self,
                              name: NSNotification.Name.UIKeyboardWillHide,
                              object: nil)
        
        target?.removeGestureRecognizer(tapGesture)
    }
    
    weak var target: UIScrollView?
    private(set) var tapGesture = UITapGestureRecognizer()
    
    var minimumOffsetPadding: CGFloat = 20
    
    fileprivate(set) var isKeyboardVisible: Bool = false
    fileprivate var defaultInsets: UIEdgeInsets
    
    var animationHandler: Empty?
    var transitionCompletion: Empty?
    
    init(target: UIScrollView) {
        self.target = target
        defaultInsets = target.contentInset
        super.init()
        tapGesture.addTarget(self, action: #selector(self.tapPerformed))
        tapGesture.delegate = self
        tapGesture.cancelsTouchesInView = false
        target.addGestureRecognizer(tapGesture)
        register()
    }
}

extension KeyboardHandler {
    
    @objc func tapPerformed() {
        target?.endEditing(true)
    }
    
    func register() {
        
        let center = NotificationCenter.default
        
        center.addObserver(self,
                           selector: #selector(self.keyboardWillShow),
                           name: NSNotification.Name.UIKeyboardWillShow,
                           object: nil)
        
        center.addObserver(self,
                           selector: #selector(self.keyboardWillHide),
                           name: NSNotification.Name.UIKeyboardWillHide,
                           object: nil)
    }
    
    func handleKeyboard(with notification: Notification) {
        
        guard let target = self.target else { return }
        guard let info = notification.userInfo else { return }
        guard let keyboardRect = info[UIKeyboardFrameEndUserInfoKey] as? CGRect else { return }
        guard let duration = info[UIKeyboardAnimationDurationUserInfoKey] as? Double else {
            return
        }
        
        guard let responder = findViewThatIsFirstResponder(parent: target) else { return }
        let contentInsets = _updatedInsetsWithKeyboard(height: keyboardRect.height)
        
        //TODO: notify keyboardWillUpdateStateTo:
        let animation: Empty = {
            DispatchQueue.toMain {
                //TODO: check if handling is enabled
                target.contentInset = contentInsets
                target.scrollIndicatorInsets = contentInsets
                let visibleHeight = target.bounds.height - contentInsets.top - contentInsets.bottom
                
                if contentInsets.bottom > 0 {
                    
                    let maxY = self._bottomOffset(with: responder,
                                                  visibleHeight: visibleHeight)
                    
                    let nextOffset = CGPoint(x: target.contentOffset.x,
                                             y: maxY)
                    target.setContentOffset(nextOffset, animated: true)
                }
            }
            self.animationHandler?()
        }
        _handleKeyboardAnimated(duration: duration, animation)
    }
    
    @objc func keyboardWillShow(_ notification: Notification) {
        
        guard !isKeyboardVisible else { return }
        isKeyboardVisible = true
        handleKeyboard(with: notification)
    }
    
    @objc func keyboardWillHide(_ notification: Notification) {
        
        guard isKeyboardVisible else { return }
        isKeyboardVisible = false
        handleKeyboard(with: notification)
    }
    
    func findViewThatIsFirstResponder(parent: UIView) -> UIView? {
        
        guard !parent.isFirstResponder else { return parent }
        
        for subview in parent.subviews {
            if let first = findViewThatIsFirstResponder(parent: subview) {
                if first.isFirstResponder { return first }
            }
        }
        return nil
    }
    
    func _updatedInsetsWithKeyboard(height: CGFloat) -> UIEdgeInsets {
        
        guard isKeyboardVisible else { return defaultInsets }
        guard let insets = target?.contentInset else { return defaultInsets }
        
        //TODO: 0 to insets
        return UIEdgeInsetsMake(insets.top, 0, insets.bottom + height, 0)
    }
    
    func _bottomOffset(with view: UIView, visibleHeight: CGFloat) -> CGFloat {
        
        guard let target = self.target else { return 0 }
        
        let contentSize = target.contentSize
        var padding: CGFloat = 0.0
        var offset: CGFloat = 0.0
        let subviewRect = view.convert(view.bounds, to: target)
        
        let centerViewInViewableArea: Empty = {
            padding = (visibleHeight - subviewRect.size.height) / 2
            
            if padding < self.minimumOffsetPadding {
                padding = self.minimumOffsetPadding
            }
            offset = subviewRect.origin.y - padding - target.contentInset.top
        }
        
        if let textInput = view as? UITextInput {
            
            if let caretPosition = textInput.selectedTextRange?.start {
                
                let position = textInput.caretRect(for: caretPosition)
                let caretRect = target.convert(position, from: view)
                
                padding = (visibleHeight - caretRect.height) / 2
                
                if padding < minimumOffsetPadding {
                    padding = minimumOffsetPadding
                }
                offset = caretRect.origin.y - padding - target.contentInset.top
            } else {
                centerViewInViewableArea()
            }
        } else {
            centerViewInViewableArea()
        }
        
        let maxOffset = contentSize.height - visibleHeight - target.contentInset.top
        
        if offset > maxOffset {
            offset = maxOffset
        }
        
        if offset < -target.contentInset.top {
            offset = target.contentInset.top
        }
        
        return offset
    }
    
    func _handleKeyboardAnimated(duration: TimeInterval, _ handler: @escaping Empty) {
        DispatchQueue.toMain {
            UIView.animate(withDuration: duration, animations: handler, completion: { _ in
                self.transitionCompletion?()
            })
        }
    }
}

extension KeyboardHandler: UIGestureRecognizerDelegate {
    
    public func gestureRecognizer(_ gestureRecognizer: UIGestureRecognizer, shouldReceive touch: UITouch) -> Bool {
        guard let superView = touch.view?.superview else { return true }
        
        return !superView.isFirstResponder
    }
}
