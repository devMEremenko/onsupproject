//
//  String+Locale.swift
//  OnSup
//
//  Created by Maxim Eremenko.
//  Copyright © 2019 OnSup. All rights reserved.
//

import Foundation

extension String {
    
    static func loc(_ title: String, comment: String? = nil) -> String {
        return NSLocalizedString(title, comment: comment ?? "")
    }
}
